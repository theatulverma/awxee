**Creating a custom EE for AWX**

- Docker installed and running locally [ Docker Desktop - docker 20.10.5 ]
- python3 [ 3.9.2 ]
- An Image Registry [ free quay.io account ]
- AWX [ 19.0.0 on minikube 1.19.0 ]

-------------------------------------------------------------------------------------------
**Setup ansible-builder**
Create a new clean python venv:

- mkdir ~/ansible-builder && cd ~/ansible-builder
- python -m venv builder
- source builder/bin/activate

**Install the components:**

- pip install ansible
- pip install ansible-builder==1.0.0.0a1

**Create the config:**

cat execution-environment.yml

> ---
> version: 1
> dependencies:
>   galaxy: requirements.yml
>   python: requirements.txt
>   system: bindep.txt
> 
> 
> additional_build_steps:
>   append:
>     - RUN alternatives --set python /usr/bin/python3
>     - COPY --from=quay.io/project-receptor/receptor:0.9.7 /usr/bin/receptor /usr/bin/receptor
>     - RUN mkdir -p /var/run/receptor
>     - ADD run.sh /run.sh
>     - CMD /run.sh
>     - USER 1000 
>     - RUN git lfs install
------------------------------------------------------------------------------------
**create file requirements.yml**

vim requirements.yml

---
collections:
  - community.general

--------------------------------------------------------------------------------------
**create file cat requirements.txt**

vim requirements.txt

urllib3
git+https://github.com/ansible/ansible-builder.git@devel#egg=ansible-builder

------------------------------------------------------------------------------------------------------
**create file bindep.txt**

vim bindep.txt

python38-devel [platform:rpm compile]
subversion [platform:rpm]
subversion [platform:dpkg]
git-lfs [platform:rpm]

---------------------------------------------------------------------------------------------------
 create dir context and inside the dir create run.sh


vim context/run.sh

#! /bin/bash
ansible-runner worker --private-data-dir=/runner

------------------------------------------------------------------------


chmod +x context/run.sh

--------------------------------------------
**Build the EE Image**

ansible-builder build --tag quay.io/atul/awx-custom-ee:latest --context ./context --container-runtime docker

===============================================================================================================
output
Running command:
  docker build -f ./context/Dockerfile -t quay.io/philgriffiths/awx-custom-ee:latest ./context
Running command:
  docker run --rm -v /Users/pgriffit/ansible-builder/builder/lib/python3.9/site-packages/ansible_builder:/ansible_builder_mount:Z quay.io/philgriffiths/awx-custom-ee:latest python3 /ansible_builder_mount/introspect.py
Running command:
  docker build -f ./context/Dockerfile -t quay.io/philgriffiths/awx-custom-ee:latest ./context
Complete! The build context can be found at: /Users/pgriffit/ansible-builder/context

==================================================================================================================================
**We should now have a container image**
docker images | grep awx-custom-ee

quay.io/philgriffiths/awx-custom-ee   latest    94cb5bb21c61   1 minutes ago   701MB

----------------------------------------------------------------------------------------------------

**Push this to quay.io**

docker push quay.io/atul/awx-custom-ee:latest
The push refers to repository [quay.io/philgriffiths/awx-custom-ee]
...
----------------------------------------------------------------------------------------------------


**Setting Up AWX Environment**

Login to AWX, as per the installation instructions:

pen `minikube service awx-service --url`

login as ---> admin
password ---> minikube kubectl -- get secret awx-admin-password -o jsonpath='{.data.password}' | base64 --decode

----------------------------------------------------------------------------------------------------------------------------

minikube kubectl get events

minikube kubectl get all

**To look at the logs for each container, use:**
minikube kubectl logs awx-b5f6cf4d4-fjnj6 awx-ee <-- replace with whatever container you want to see, awx-web, awx-task, awx-ee

-------------------------------------------------------------------------------------------------------------------------=======================
